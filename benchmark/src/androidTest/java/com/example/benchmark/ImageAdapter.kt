package com.example.benchmark

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.fastimage.ImageData
import com.example.fastimage.FastImage

class ImageAdapter(
    var items: ArrayList<Pair<String?, String>>,
    private val context: Context,
    var isMine: Boolean = true
) : RecyclerView.Adapter<ImageAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_layout, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tvIndex.text = position.toString()
        if (isMine) {
            val imageData = ImageData(
                url = items[position].second,
                imageView = holder.imageView,
                blur = items[position].first,
                placeholder = R.drawable.ic_placeholder
            )
            FastImage.getInstance(context).load(imageData)
        } else {
            Glide.with(context).load(items[position].second).into(holder.imageView)
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val imageView = view.findViewById(R.id.item_imageView) as ImageView
        val tvIndex = view.findViewById(R.id.tvIndex) as TextView
    }

}