package com.example.benchmark.base

import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Lifecycle
import androidx.test.ext.junit.rules.ActivityScenarioRule
import org.junit.After

interface ActivityTestContract<T : AppCompatActivity> {
    /**
     * Annotate with [@get:Rule] on implementation
     */
    val activityScenarioRule: ActivityScenarioRule<T>

    @After
    fun finishActivity() {
        activityScenarioRule.scenario.moveToState(Lifecycle.State.DESTROYED)
    }
}
