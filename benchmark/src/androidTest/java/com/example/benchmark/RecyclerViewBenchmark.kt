package com.example.benchmark

import android.view.View
import android.view.ViewGroup
import androidx.benchmark.junit4.BenchmarkRule
import androidx.benchmark.junit4.measureRepeated
import androidx.recyclerview.widget.RecyclerView
import androidx.test.annotation.UiThreadTest
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import com.example.benchmark.base.ActivityTestContract
import com.example.benchmark.base.BenchmarkTestContract
import org.junit.Assert.assertTrue
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@LargeTest
@RunWith(AndroidJUnit4::class)
class RecyclerViewBenchmark : ActivityTestContract<BenchmarkRecyclerViewActivity>, BenchmarkTestContract {

    @get:Rule
    override val benchmarkRule = BenchmarkRule()


    @get:Rule
    override val activityScenarioRule = ActivityScenarioRule(BenchmarkRecyclerViewActivity::class.java)

    @Test
    @UiThreadTest
    fun scrollRecyclerViewByFastImage() {
        activityScenarioRule.scenario.onActivity { activity ->
            activity.adapter.isMine = true
            activity.adapter.notifyDataSetChanged()
            assertTrue("RecyclerView expected to have children", activity.binding.rvImages.childCount > 0)
            benchmarkRule.measureRepeated {
                activity.binding.rvImages.scrollBy(0, activity.binding.rvImages.getLastChild().height)
            }
        }
    }

    @Test
    @UiThreadTest
    fun scrollRecyclerViewByGlide() {
        activityScenarioRule.scenario.onActivity { activity ->
            activity.adapter.isMine = false
            activity.adapter.notifyDataSetChanged()
            assertTrue("RecyclerView expected to have children", activity.binding.rvImages.childCount > 0)
            benchmarkRule.measureRepeated {
                activity.binding.rvImages.scrollBy(0, activity.binding.rvImages.getLastChild().height)
            }
        }
    }
    
    private fun ViewGroup.getLastChild(): View = getChildAt(childCount - 1)

}