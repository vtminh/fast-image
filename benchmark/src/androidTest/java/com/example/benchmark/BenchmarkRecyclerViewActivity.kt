/*
 * Copyright 2019 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.benchmark

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.benchmark.databinding.ActivityBenchmarkRecyclerviewBinding

class BenchmarkRecyclerViewActivity : AppCompatActivity() {
    private val imageList = arrayListOf<Pair<String?, String>>().apply {
        for (i in 0..10000) {
            this.add(Pair<String?, String>(null, "https://picsum.photos/id/$i/4928/3264"))
        }
    }

    lateinit var binding: ActivityBenchmarkRecyclerviewBinding
    lateinit var adapter: ImageAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityBenchmarkRecyclerviewBinding.inflate(layoutInflater)
        setContentView(binding.root)
        adapter = ImageAdapter(imageList, this)
        binding.rvImages.layoutManager = LinearLayoutManager(this)
        binding.rvImages.adapter = adapter
    }
}