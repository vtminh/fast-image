package com.example.benchmark

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import androidx.benchmark.junit4.BenchmarkRule
import androidx.benchmark.junit4.measureRepeated
import androidx.test.annotation.UiThreadTest
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import com.example.benchmark.base.ActivityTestContract
import com.example.benchmark.base.BenchmarkTestContract
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@LargeTest
@RunWith(AndroidJUnit4::class)
class MemoryCacheBenchMark : ActivityTestContract<BenchmarkMemoryCacheActivity>, BenchmarkTestContract {

    companion object {
        private const val KEY_TEST = "KEY_TEST"
    }

    @get:Rule
    override val benchmarkRule = BenchmarkRule()


    @get:Rule
    override val activityScenarioRule = ActivityScenarioRule(BenchmarkMemoryCacheActivity::class.java)

    @Before
    fun setup() {
        activityScenarioRule.scenario.onActivity { activity ->
            val bitmap: Bitmap = BitmapFactory.decodeResource(
                activity.resources,
                R.drawable.ic_placeholder
            )
            activity.memoryCache.put(KEY_TEST, bitmap)
            activity.diskCache.put(KEY_TEST, bitmap)
        }
    }

    @Test
    @UiThreadTest
    fun loadBitmapFromMemoryByFastImage() {
        activityScenarioRule.scenario.onActivity { activity ->
            benchmarkRule.measureRepeated {
                val cachedBitmap = activity.memoryCache.get(KEY_TEST)
                Assert.assertNotNull("Non null", cachedBitmap)
                cachedBitmap?.let { activity.binding.ivImage.setImageBitmap(it) }
            }
        }
    }

    @Test
    @UiThreadTest
    fun loadBitmapFromDiskByFastImage() {
        activityScenarioRule.scenario.onActivity { activity ->
            benchmarkRule.measureRepeated {
                val cachedBitmap = activity.diskCache.get(KEY_TEST)
                Assert.assertNotNull("Non null", cachedBitmap)
                cachedBitmap?.let { activity.binding.ivImage.setImageBitmap(it) }
            }
        }
    }
}