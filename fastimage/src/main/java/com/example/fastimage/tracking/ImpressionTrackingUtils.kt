package com.example.fastimage.tracking

import android.graphics.Rect
import android.os.Handler
import android.os.Looper
import android.util.Log
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

interface OnImpressionTrackingListener {
    fun onItemSeen(pos: Int)
}

object ImpressionTrackingUtils {
    private const val TAG = "ImpressionTrackingUtils"
    private const val MIN_VISIBLE_PERCENTAGE = 50
    private const val MIN_VISIBLE_TIME = 3000L //1s

    fun RecyclerView.addOnImpressionTrackingListener(
        minVisiblePercentage: Int = MIN_VISIBLE_PERCENTAGE,
        minVisibleTime: Long = MIN_VISIBLE_TIME,
        listener: OnImpressionTrackingListener
    ) {
        this.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    Log.i(TAG, "Stop scrolling. Start tracking visible items")
                    startTracking(this@addOnImpressionTrackingListener, minVisiblePercentage, minVisibleTime, listener)
                }
            }
        })

        this.viewTreeObserver
            .addOnGlobalLayoutListener {
                Log.i(TAG, "First showing. Start tracking visible items")
                startTracking(this@addOnImpressionTrackingListener, minVisiblePercentage, minVisibleTime, listener)
            }
    }

    private fun startTracking(recyclerView: RecyclerView, minVisiblePercentage: Int, minVisibleTime: Long, listener: OnImpressionTrackingListener) {
        val layoutManager = recyclerView.layoutManager as LinearLayoutManager
        val firstPosition = layoutManager.findFirstVisibleItemPosition()
        val lastPosition = layoutManager.findLastVisibleItemPosition()
        val rvRect = Rect()
        recyclerView.getGlobalVisibleRect(rvRect)
        for (pos in firstPosition..lastPosition) {
            val rowRect = Rect()
            val isVisible = layoutManager.findViewByPosition(pos)?.getGlobalVisibleRect(rowRect) ?: false
            if (!isVisible) {
                break
            }

            if (layoutManager.canScrollHorizontally()) {
                val viewWidth = layoutManager.findViewByPosition(pos)?.width ?: 0
                var horizontalPercentage: Int
                horizontalPercentage = if (rowRect.right >= rvRect.right) {
                    val visibleWidthFirst: Int = rvRect.right - rowRect.left
                    visibleWidthFirst * 100 / viewWidth
                } else {
                    val visibleWidthFirst: Int = rowRect.right - rvRect.left
                    visibleWidthFirst * 100 / viewWidth
                }

                if (horizontalPercentage > 100) horizontalPercentage = 100
                if (horizontalPercentage >= minVisiblePercentage) {
                    onItemImpression(layoutManager, pos, minVisibleTime, listener)
                }
            } else {
                val viewHeight: Int = layoutManager.findViewByPosition(pos)?.height ?: 0
                var verPercentage: Int
                verPercentage = if (rowRect.bottom >= rvRect.bottom) {
                    val visibleHeightFirst: Int = rvRect.bottom - rowRect.top
                    visibleHeightFirst * 100 / viewHeight
                } else {
                    val visibleHeightFirst: Int = rowRect.bottom - rvRect.top
                    visibleHeightFirst * 100 / viewHeight
                }
                if (verPercentage > 100) verPercentage = 100
                if (verPercentage >= minVisiblePercentage) {
                    onItemImpression(layoutManager, pos, minVisibleTime, listener)
                }
            }
        }
    }

    private fun onItemImpression(
        layoutManager: LinearLayoutManager,
        pos: Int,
        minVisibleTime: Long,
        listener: OnImpressionTrackingListener
    ) {

        Handler(Looper.getMainLooper()).postDelayed({
            val firstPosition = layoutManager.findFirstVisibleItemPosition()
            val lastPosition = layoutManager.findLastVisibleItemPosition()
            if (pos in firstPosition..lastPosition) {
                listener.onItemSeen(pos)
                Log.i(TAG, "Item $pos is still visible after $minVisibleTime ms. Track it.")

            } else {
                Log.i(TAG, "Item $pos is no longer visible. Don't track.")
            }
        }, minVisibleTime)
    }
}