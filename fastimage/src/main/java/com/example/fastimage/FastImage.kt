package com.example.fastimage

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Log
import android.view.View
import android.widget.ImageView
import com.example.fastimage.Constants.DISK_CACHE_DIRECTORY
import com.example.fastimage.blur.BlurHash
import com.example.fastimage.blur.BlurHashDecoder
import com.example.fastimage.cache.DoubleCache
import com.example.fastimage.cache.ImageCache
import com.example.fastimage.cache.MemoryCache
import com.example.fastimage.transformation.TransformedImage.Companion.loadTransformedImage
import com.example.fastimage.utils.BitmapUtils.downloadBitmapFromUrl
import com.example.fastimage.utils.SingletonHolder
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.File
import java.util.Collections
import java.util.Collections.synchronizedMap
import java.util.WeakHashMap
import java.util.concurrent.ConcurrentHashMap

class FastImage(private val context: Context) {

    companion object : SingletonHolder<FastImage, Context>(object : (Context) -> FastImage {
        override fun invoke(p1: Context): FastImage {
            return FastImage(p1.applicationContext)
        }
    })

    private val TAG = "FastImage"

    private val scope = CoroutineScope(SupervisorJob() + Dispatchers.Main.immediate + CoroutineExceptionHandler { _, throwable ->
        Log.e(TAG, "CoroutineExceptionHandler", throwable)
    })

    private var cache: ImageCache = DoubleCache(File(context.cacheDir, DISK_CACHE_DIRECTORY))
    private var blurCache = MemoryCache()

    private val imageViewMap = synchronizedMap(WeakHashMap<ImageView, String>())
    private val downloadingUrl = Collections.newSetFromMap(ConcurrentHashMap<String, Boolean>())

    fun load(imageData: ImageData) = with(imageData) {
        val job = scope.launch {
            imageViewMap[imageView] = url
            imageData.placeholder?.let {
                if (!isActive) return@launch
                val placeholderBitmap = BitmapFactory.decodeResource(context.resources, it)
                setBitmapToImageView(this@with, placeholderBitmap)
            }

            if (!isActive) return@launch
            val bitmap = getImageInCache(url)

            if (bitmap != null) {
                if (!isActive) return@launch
                setBitmapToImageView(imageData, bitmap)
            } else {
                blur?.let { loadBlur(imageData, it) }
                Log.i(TAG, "Load bitmap from remote")
                if (!isActive) return@launch
                downloadBitmap(imageData, imageView)
            }
        }

        registerJobCancellationListener(imageData, job)
    }

    private suspend fun loadBlur(imageData: ImageData, blur: String) {
        blurCache.get(imageData.url)?.let {
            setBitmapToImageView(imageData, it)
        } ?: kotlin.run {
            imageData.imageView.post {
                val job = scope.launch {
                    val width = withContext(Dispatchers.Main.immediate) { imageData.imageView.measuredWidth }
                    val height = withContext(Dispatchers.Main.immediate) { imageData.imageView.measuredHeight }
                    val blurBitmap = withContext(Dispatchers.IO) {
                        BlurHashDecoder.decode(blur, width, height, 1f, false)
                    }
                    if (!isActive) return@launch
                    blurBitmap?.let {
                        blurCache.put(imageData.url, it)
                        setBitmapToImageView(imageData, it)
                    }
                }
                registerJobCancellationListener(imageData, job)

            }
        }
    }

    private suspend fun downloadBitmap(imageData: ImageData, imageView: ImageView) {
        if (downloadingUrl.contains(imageData.url)) {
            Log.i(TAG, "The URL ${imageData.url} is being downloaded. Skip it. ")

            return
        }
        if (isImageViewRecycled(imageData)) {
            Log.i(TAG, "RecyclerView item is recycled, stop downloading")
            return
        }

        imageData.imageView.post {
            val job = scope.launch(Dispatchers.Default) {
                Log.i(TAG, "Start download ${imageData.url} ${imageView.width} ${imageView.height}")
                downloadingUrl.add(imageData.url)
                downloadBitmapFromUrl(imageData.url, imageView.width, imageView.height)?.let { bitmap ->
                    if (!isActive) return@launch
                    cache.put(imageData.url, bitmap)
                    blurCache.remove(imageData.url)
                    Log.i(TAG, "url = ${imageData.url} blur = ${BlurHash.encode(bitmap, bitmap.width, bitmap.height)}")
                    if (!isActive) return@launch
                    setBitmapToImageView(imageData, bitmap)
                    downloadingUrl.remove(imageData.url)
                }
            }
            registerJobCancellationListener(imageData, job)
        }
    }

    private suspend fun setBitmapToImageView(imageData: ImageData, bitmap: Bitmap) {
        if (isImageViewRecycled(imageData)) return

        if (imageData.transformation != null) {
            val transformedDrawable = withContext(Dispatchers.IO) {
                imageData.transformation.transform(bitmap)
            }
            withContext(Dispatchers.Main.immediate) {
                imageData.imageView.loadTransformedImage(transformedDrawable)
            }
        } else {
            withContext(Dispatchers.Main.immediate) {
                imageData.imageView.setImageBitmap(bitmap)
            }
        }
    }

    private suspend fun getImageInCache(imageUrl: String): Bitmap? {
        return withContext(Dispatchers.IO) {
            this@FastImage.cache.get(imageUrl)
        }
    }

    private fun isImageViewRecycled(imageData: ImageData): Boolean {
        val url = imageViewMap[imageData.imageView]
        return url == null || url != imageData.url
    }

    private fun registerJobCancellationListener(imageData: ImageData, job: Job) {
        imageData.imageView.addOnAttachStateChangeListener(object : View.OnAttachStateChangeListener {
            override fun onViewAttachedToWindow(v: View?) {
            }

            override fun onViewDetachedFromWindow(v: View?) {
                Log.i(TAG, "onViewDetachedFromWindow is called, cancel job")
                downloadingUrl.remove(imageData.url)
                job.cancel()
            }
        })
    }
}


