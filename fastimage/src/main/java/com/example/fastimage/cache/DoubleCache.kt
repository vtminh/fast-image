package com.example.fastimage.cache

import android.graphics.Bitmap
import android.util.Log
import java.io.File

class DoubleCache(cacheDir: File) : ImageCache {

    private val memCache = MemoryCache()
    private val diskCache = DiskCache(cacheDir)

    override suspend fun get(key: String): Bitmap? {
        Log.i("FastImage", if (memCache.get(key) != null) "Load bitmap from Memory" else if(diskCache.get(key) != null ) "Load bitmap from Disk" else "No data from cache")
        return memCache.get(key) ?: run {
            diskCache.get(key)?.apply {
                memCache.put(key, this)
            }
        }
    }

    override suspend fun remove(key: String) {
        memCache.remove(key)
        diskCache.remove(key)
    }

    override suspend fun put(key: String, bitmap: Bitmap) {
        memCache.put(key, bitmap)
        diskCache.put(key, bitmap)
    }

    override suspend fun clear() {
        memCache.clear()
        diskCache.clear()
    }
}