package com.example.fastimage.cache

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Log
import com.example.fastimage.Constants.DISK_CACHE_MAX_SIZE
import com.example.fastimage.md5
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.util.TreeMap
import java.util.concurrent.ConcurrentHashMap

class DiskCache(
    private val cacheDir: File,
    private val size: Int = DISK_CACHE_MAX_SIZE
) : ImageCache {

    private val TAG = "DiskCache"

    private val lruCache = object : LinkedHashMap<String, Long>(size, 0.75f, true) {
        override fun removeEldestEntry(eldest: MutableMap.MutableEntry<String, Long>): Boolean {
            val keyMd5 = eldest.key.md5()
            val eldestFile = File(keyMd5)
            eldestFile.delete()

            Log.i(TAG, "Remove file ${eldest.key}")
            return super.removeEldestEntry(eldest)
        }
    }.apply {
        //Add all file info, sorted by lastModified
        val sortedInfoList: TreeMap<String, Long> = TreeMap()
        cacheDir.listFiles()?.forEach { file ->
            sortedInfoList[file.name.substring(file.name.lastIndexOf("/") + 1)] = file.lastModified()
        }
        this.putAll(sortedInfoList)
    }

    private val locks = ConcurrentHashMap<String, Mutex>()

    override suspend fun get(key: String): Bitmap? {
        Log.i(TAG, "get $key")
        val keyMd5 = key.md5()
        lruCache[keyMd5]
        val file = File(cacheDir, keyMd5)
        return if (file.exists()) {
            file.setLastModified(System.currentTimeMillis())
            BitmapFactory.decodeFile(file.path)
        } else {
            null
        }
    }

    @Suppress("BlockingMethodInNonBlockingContext")
    override suspend fun put(key: String, bitmap: Bitmap) {
        val mutex = locks.getOrPut(key) {
            Mutex()
        }
        mutex.withLock {
            Log.i(TAG, "put $key")
            val keyMd5: String = key.md5()
            val f = File(cacheDir, keyMd5)
            if (f.parentFile?.exists() == false) {
                f.parentFile?.mkdirs()
            }

            if (f.exists()) return

            lruCache[keyMd5] = System.currentTimeMillis()

            try {
                f.createNewFile()
                val bos = ByteArrayOutputStream()
                bitmap.compress(Bitmap.CompressFormat.PNG, 0, bos)
                val bitmapData: ByteArray = bos.toByteArray()


                val fos = FileOutputStream(f)
                fos.write(bitmapData)
                fos.flush()
                fos.close()

            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }
        }

    }

    override suspend fun remove(key: String) {
        val keyMd5: String = key.md5()
        try {
            val f = File(cacheDir, keyMd5)
            f.delete()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override suspend fun clear() {
        cacheDir.delete()
    }

}