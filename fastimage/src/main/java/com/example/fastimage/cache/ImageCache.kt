package com.example.fastimage.cache

import android.graphics.Bitmap

interface ImageCache {
    suspend fun put(key: String, bitmap: Bitmap)
    suspend fun get(key: String): Bitmap?
    suspend fun remove(key: String)
    suspend fun clear()
}