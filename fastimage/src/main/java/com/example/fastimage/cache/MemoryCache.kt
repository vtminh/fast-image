package com.example.fastimage.cache

import android.graphics.Bitmap
import android.util.LruCache
import com.example.fastimage.Constants.MEM_CACHE_RATIO

class MemoryCache : ImageCache {
     private val cache: LruCache<String, Bitmap>
    private var maxMemory: Long = Runtime.getRuntime().maxMemory() / 1024

    init {
        val cacheSize: Int = (maxMemory / MEM_CACHE_RATIO).toInt()
        cache = object : LruCache<String, Bitmap>(cacheSize) {
            override fun sizeOf(key: String?, bitmap: Bitmap?): Int {
                return (bitmap?.byteCount ?: 0) / 1024

            }
        }
    }

    override suspend fun put(key: String, bitmap: Bitmap) {
        cache.put(key, bitmap)
    }

    override suspend fun get(key: String): Bitmap? {
        return cache.get(key)
    }

    override suspend fun remove(key: String) {
        cache.remove(key)
    }

    override suspend fun clear() {
        cache.evictAll()
    }
}