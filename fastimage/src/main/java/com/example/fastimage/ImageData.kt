package com.example.fastimage

import android.widget.ImageView
import com.example.fastimage.transformation.TransformedImage
import com.example.fastimage.transformation.Transformation

class ImageData(
    var url: String,
    var imageView: ImageView,
    var blur: String? = null,
    var placeholder: Int? = null,
    val transformation: Transformation? = null
)



