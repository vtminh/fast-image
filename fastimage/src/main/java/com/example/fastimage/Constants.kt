package com.example.fastimage

object Constants {
    const val DISK_CACHE_MAX_SIZE = 100 //100 files
    const val MEM_CACHE_RATIO = 4 // mem cache should be equal to a half of available memory at runtime
    const val DISK_CACHE_DIRECTORY = "fastimage"

}