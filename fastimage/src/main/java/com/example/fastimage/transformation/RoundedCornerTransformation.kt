package com.example.fastimage.transformation

import android.content.res.Resources
import android.graphics.Bitmap
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory

class RoundedCornerTransformation(
    private val resources: Resources,
    private val radius: Float
) : Transformation {

    override fun transform(bitmap: Bitmap): TransformedImage {
        val roundedBitmap = RoundedBitmapDrawableFactory.create(resources, bitmap)
        roundedBitmap.cornerRadius = radius
        return TransformedImage.DrawableTransformed(roundedBitmap)
    }
}
