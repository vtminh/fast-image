package com.example.fastimage.transformation

import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.widget.ImageView

interface Transformation {
    fun transform(bitmap: Bitmap): TransformedImage
}

sealed class TransformedImage {
    class BitmapTransformed(val data: Bitmap) : TransformedImage()
    class DrawableTransformed(val data: Drawable) : TransformedImage()

    companion object {
        fun ImageView.loadTransformedImage(image: TransformedImage) {
            when (image) {
                is BitmapTransformed -> {
                    this.setImageBitmap(image.data)
                }
                is DrawableTransformed -> {
                    this.setImageDrawable(image.data)
                }
            }
        }

    }
}