package com.example.fastimage.transformation

import android.content.res.Resources
import android.graphics.Bitmap
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory


class CircleTransformation(private val resources: Resources) : Transformation{
    override fun transform(bitmap: Bitmap): TransformedImage {
        val roundedBitmap = RoundedBitmapDrawableFactory.create(resources, bitmap)
        roundedBitmap.isCircular = true
        return TransformedImage.DrawableTransformed(roundedBitmap)
    }
}