package com.example.fastimage

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.fastimage.databinding.ActivityTransfromationBinding
import com.example.fastimage.transformation.CircleTransformation
import com.example.fastimage.transformation.RoundedCornerTransformation

class TransformationActivity : AppCompatActivity() {

    lateinit var binding: ActivityTransfromationBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityTransfromationBinding.inflate(layoutInflater)
        setContentView(binding.root)

        FastImage(this).load(
            ImageData(
                url = "https://homepages.cae.wisc.edu/~ece533/images/sails.png",
                imageView = binding.ivCircle,
                transformation = CircleTransformation(resources)
            )
        )

        FastImage(this).load(
            ImageData(
                url = "https://homepages.cae.wisc.edu/~ece533/images/sails.png",
                imageView = binding.ivRoundedCorner,
                transformation = RoundedCornerTransformation(resources, 20f)
            )
        )
    }
}