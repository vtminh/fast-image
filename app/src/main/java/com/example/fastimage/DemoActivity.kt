package com.example.fastimage

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.fastimage.databinding.ActivityDemoBinding

class DemoActivity : AppCompatActivity() {

    lateinit var binding: ActivityDemoBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityDemoBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.tvRecyclerView.setOnClickListener {
            val intent = Intent(this, RecyclerViewActivity::class.java)
            startActivity(intent)
        }

        binding.tvTransformation.setOnClickListener {
            val intent = Intent(this, TransformationActivity::class.java)
            startActivity(intent)
        }
    }

}