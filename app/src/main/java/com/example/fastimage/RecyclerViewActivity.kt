package com.example.fastimage

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.fastimage.databinding.ActivityRecyclerviewBinding
import com.example.fastimage.tracking.ImpressionTrackingUtils.addOnImpressionTrackingListener
import com.example.fastimage.tracking.OnImpressionTrackingListener

class RecyclerViewActivity : AppCompatActivity() {

    private val imageList = arrayListOf<Pair<String?, String>>(
        Pair("U8HLC;|:2#t*2s9b}9RUM@S}~Ei_L#\${%gxr", "https://homepages.cae.wisc.edu/~ece533/images/baboon.png"),
        Pair("UHQ9_@~V01MwD*oH4nD%0LjXIAE1of-oNHxt", "https://homepages.cae.wisc.edu/~ece533/images/arctichare.png"),
        Pair("UBFFss%MD%t7~q%MD%Rj4nD%IUof4nIUxuof", "https://homepages.cae.wisc.edu/~ece533/images/barbara.png"),
        Pair("U9F~gc_3D%WB~qM{WBRj?bIUofM{9FofofWB", "https://homepages.cae.wisc.edu/~ece533/images/boat.png"),
        Pair("U3Cs{:F80e%x=;Otr=?F00oG%j9F0ooM\$\$-q", "https://homepages.cae.wisc.edu/~ece533/images/sails.png"),
        Pair("U6BpnU?bxu-;M{D%9FD%D%xuIUM{~qRj9Ft7", "https://homepages.cae.wisc.edu/~ece533/images/zelda.png"),
        Pair("U3AAXK%~_M\$%008w9Gx[00?vD*IUT1RP-ojY", "https://homepages.cae.wisc.edu/~ece533/images/watch.png"),
        Pair("U8Gkg.}]02E,xbX5-T+^0L~BFyE+^QwN\$w=?", "https://homepages.cae.wisc.edu/~ece533/images/tulips.png"),
        Pair("UjIXj[OZNMoL#qwaRiS7BHxtodRkE2Nbt7xZ", "https://homepages.cae.wisc.edu/~ece533/images/serrano.png"),
        Pair("U3Cs{:F80e%x=;Otr=?F00oG%j9F0ooM\$\$-q", "https://homepages.cae.wisc.edu/~ece533/images/sails.png"),
        Pair("U15ruA5s0c+bqOx=9]v%00wZ=%k;--H#\$xK|", "https://homepages.cae.wisc.edu/~ece533/images/pool.png"),
        Pair("UTI=Mq?bM{Rj_3of%MRj~qxut7WBxuayxuIU", "https://homepages.cae.wisc.edu/~ece533/images/mountain.png"),
        Pair("UOBND[?bozjt_4x]ogofIURjj@t7DiM_RjWB", "https://images.pexels.com/photos/1722183/pexels-photo-1722183.jpeg?dl&fit=crop&crop=entropy&w=1280&h=1920"),
        Pair("UQB:Nga}%LxaIpR*IoWB~UWVt7t6xtofs:j[", "https://i.picsum.photos/id/237/1000/1500.jpg?hmac=Amn0qT-76ewlDFh7asmagMMTwl9nnMOvVyfX0NJ9afE"),
        Pair("U47AuyH;x9xy00NYS2W.}P%PjeV;0xR*Wrju", "https://images.pexels.com/photos/1470405/pexels-photo-1470405.jpeg?dl&fit=crop&crop=entropy&w=1280&h=853"),
        Pair("UXG0IOo1ShozLNs8\$%ofV]RPR*WBEkaeaKe.", "https://images.pexels.com/photos/1005417/pexels-photo-1005417.jpeg?dl&fit=crop&crop=entropy&w=1280&h=1600"),
        Pair("UAC=A6sW00xAS%W==aR*00Nu~WwyxAwMI[kS", "https://images.pexels.com/photos/1294671/pexels-photo-1294671.jpeg?dl&fit=crop&crop=entropy&w=1280&h=1920"),
        Pair("U597CP4mRk-=00R*xuRj0K~qM_D%IpM{X8xZ", "https://images.pexels.com/photos/1040893/pexels-photo-1040893.jpeg?dl&fit=crop&crop=entropy&w=1280&h=1919"),
        Pair("UaIDz6~AI:Ioxut7Rjodn,V[j@WBELbIxZa#", "https://images.pexels.com/photos/1956974/pexels-photo-1956974.jpeg?dl&fit=crop&crop=entropy&w=1280&h=853"),
        Pair("UUGu?w%g9FWB_4%MRjs:?HxaR-t7%MxtRjbH", "https://images.pexels.com/photos/1374064/pexels-photo-1374064.jpeg?dl&fit=crop&crop=entropy&w=1280&h=1706"),
        Pair("UAEV_^%e^*EL+1E2xZt6.6s:kVsD_LWr%1Ns", "https://images.pexels.com/photos/1931142/pexels-photo-1931142.jpeg?dl&fit=crop&crop=entropy&w=1280&h=1919"),
        Pair("U54yo~.TIAtSyEt7t7tR4TMd-;WWMdRPNGM{", "https://images.pexels.com/photos/1295036/pexels-photo-1295036.jpeg?dl&fit=crop&crop=entropy&w=1280&h=1707"),
        Pair("UWAo+u_L\$kyDR,SeV?aKMxRQn%j[VsVtRjWB", "https://images.pexels.com/photos/1320684/pexels-photo-1320684.jpeg?dl&fit=crop&crop=entropy&w=1920&h=1440"),
        Pair("UkJt}4j[IUWB~qWCxaofozj[WBayM{jtkCfk", "https://images.pexels.com/photos/1908677/pexels-photo-1908677.jpeg?dl&fit=crop&crop=entropy&w=1280&h=1706")
    ).apply {
        for (i in 0..400) {
            this.add(Pair<String?, String>(null, "https://picsum.photos/id/$i/4928/3264"))
        }
    }

    private val TAG = "RecyclerViewActivity"
    lateinit var binding: ActivityRecyclerviewBinding
    lateinit var adapter: ImageAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityRecyclerviewBinding.inflate(layoutInflater)
        setContentView(binding.root)
        adapter = ImageAdapter(imageList, this)
        binding.rvImages.layoutManager = LinearLayoutManager(this)
        binding.rvImages.adapter = adapter

        binding.switchLib.setOnCheckedChangeListener { buttonView, isChecked ->
            adapter.isMine = isChecked
            adapter.notifyDataSetChanged()
            if (isChecked) {
                binding.switchLib.text = getString(R.string.str_mine)
            } else {
                binding.switchLib.text = getString(R.string.str_theirs)
            }
        }

        binding.rvImages.addOnImpressionTrackingListener(listener = object : OnImpressionTrackingListener {
            override fun onItemSeen(pos: Int) {
                Log.i(TAG, "Item $pos is impressive. Send Tracking")
            }
        })
    }
}